#!/usr/bin/python3

# project path
from sys import path
path.append(
    "/home/mjablonski/Projekty/freelance"
    "/medialab_event_processor")

# imports
import unittest
from medialab_event_processor.medialab_event_processor import PlaceGeocoder


# class
class TestGeocoder(unittest.TestCase):

    def test_addres_before_place_name(self):
        place_geocoder = PlaceGeocoder()
        location = place_geocoder.geocode(
            'Spodek', 'Mariacka 10', 'Katowice')
        self.assertEqual(
            str(location),
            '10, Mariacka, Śródmieście, '
            'Katowice, śląskie, 40-014, RP')

    def test_city_query(self):
        place_geocoder = PlaceGeocoder()
        location = place_geocoder.geocode('', '', 'Katowice')
        self.assertEqual(
            str(location),
            'Katowice, śląskie, RP')

    def test_reduntant_strings(self):
        place_geocoder = PlaceGeocoder()

        location_1 = place_geocoder.geocode(
            '', 'Rondo im. gen. Jerzego Ziętka 1', 'Katowice')
        self.assertEqual(
            str(location_1),
            '1, rondo Generała Jerzego Ziętka, '
            'Koszutka, Katowice, śląskie, 41-101, RP')

        location_2 = place_geocoder.geocode(
            '', 'ul. 3 Maja 31 a', 'Katowice')
        self.assertIn(
            '31A, 3 Maja, '
            'Śródmieście, Katowice, śląskie, 40-096, RP',
            str(location_2))

        location_3 = place_geocoder.geocode(
            '', 'ul. św. Jana 10', 'Katowice')
        self.assertIn(
            '10, Świętego Jana, Śródmieście, '
            'Katowice, śląskie, 40-951, RP',
            str(location_3))

        location_4 = place_geocoder.geocode(
            '', 'al. W. Korfantego 3', 'Katowice')
        self.assertIn(
            '3, Aleja Wojciecha Korfantego, Śródmieście, '
            'Katowice, śląskie, 40-005, RP',
            str(location_4))

        location_5 = place_geocoder.geocode(
            '', 'al. Wojciecha Korfantego 6', 'Katowice')
        self.assertIn(
            '6, Aleja Wojciecha Korfantego, Śródmieście, '
            'Katowice, śląskie, 40-004, RP',
            str(location_5))

        location_6 = place_geocoder.geocode(
            '', 'ul. gen. J. Hallera 28', 'Katowice')
        self.assertIn(
            '28, Generała Józefa Hallera, Bagno, Burowiec, '
            'Szopienice-Burowiec, Katowice, śląskie, 40-335, RP',
            str(location_6))

        location_7 = place_geocoder.geocode(
            'Filharmonia Śląska, Sala im. Karola Stryji', '', 'Katowice')
        self.assertIn(
            '2, Sokolska, Załęskie Przedmieście, '
            'Śródmieście, Katowice, śląskie, 40-097, RP',
            str(location_7))

        location_8 = place_geocoder.geocode(
            'aula Śląskiej Wyższej Szkoły Zarządzania ul. Francuska 12',
            '', 'Katowice')
        self.assertIn(
            '12, Francuska, Karbowa, '
            'Śródmieście, Katowice, śląskie, 40-015, RP',
            str(location_8))


# execution
if __name__ == '__main__':
    unittest.main()
