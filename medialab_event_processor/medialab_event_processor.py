#!/usr/bin/python3

import pymysql
import os
import re
import time
import logging
import csv
import pickle
import numpy as np
from geopy.geocoders import Nominatim
from difflib import SequenceMatcher


class MysqlPipeline(object):
    """docstring for MysqlPipeline"""
    # constructor
    def __init__(self):
        self._connection = pymysql.connect(
            host=os.environ['MEDIALAB_DB_HOST'],
            port=3306, user=os.environ['MEDIALAB_DB_USER'],
            passwd=os.environ['MEDIALAB_DB_PASSWORD'],
            charset='utf8', autocommit=False)
        self._cursor = self._connection.cursor()
        self._katowice_geo_boundary_left = 18.54
        self._katowice_geo_boundary_right = 19.08
        self._katowice_geo_boundary_uppper = 54.19
        self._katowice_geo_boundary_lower = 50.08

    # public methods
    def fetch_data(self, query):
        self._cursor.execute(query)
        result = self._cursor.fetchall()
        return result

    def transfer_posts(self, source_site, check_column=None, min_value=None):
        insert_clause = (
            "insert ignore into unique.post ("
            "   post_source, post_source_id, post_url, post_title, "
            "   post_content, post_tags, event_time, event_place_name, "
            "   event_address, event_city, event_geo_lat, event_geo_lng, "
            "   event_tickets)")
        check_column_condition = f" {check_column}>='{min_value}' "
        silesiakultura_query = (
            insert_clause +
            "select "
            "   'silesiakultura' as post_source, "
            "   event_id as post_source_id, "
            "   url as post_url, "
            "   name as post_title, "
            "   description as post_content, "
            "   tags as post_tags, "
            "   time as event_time, "
            "   place_name as event_place_name, "
            "   address as event_address, "
            "   city as event_city, "
            "   null as event_geo_lat, "
            "   null as event_geo_lng,  "
            "   tickets_info as event_tickets "
            "from silesiakultura.event "
            "   join silesiakultura.place using (place_id) " +
            (f"where_{check_column_condition}"
                if min_value is not None else ''))
        if source_site == 'silesiakultura':
            self._cursor.execute(silesiakultura_query)
            self._connection.commit()
        facebook_query = (
            insert_clause +
            "select "
            "   'facebook' as post_source, "
            "   facebook_id as post_source_id, "
            "   null as post_url, "
            "   name as post_title, "
            "   description as post_content, "
            "   null as post_tags, "
            "   start_time as event_time, "
            "   place_name as event_place_name, "
            "   case "
            "       when place like '%\"street\":%' then "
            "           replace("
            "               substring_index("
            "                   substring_index(place, '\"street\":', -1), "
            "                   '\"', "
            "                   2), "
            "           '\"', "
            "           '') "
            "       else ''"
            "       end"
            "       as event_address, "
            "   replace("
            "       substring_index("
            "           substring_index(place, '\"city\":', -1), "
            "           '\"', "
            "           2), "
            "       '\"', "
            "       '') "
            "       as event_address, "
            "   place_lat as event_geo_lat, "
            "   place_lng as event_geo_lng, "
            "   null as event_tickets "
            "from sc_clean.events "
            "where "
            "   place_name not in "
            "       ('Katowice Miasto Wydarzeń', "
            "       'Katowice, Woiwodschaft Schlesien, Polen', "
            "       'Polska, Poland', 'Katowice') "
            "   and place like '%\"city\":\"Katowice%'" +
            (f"   and {check_column_condition}"
                if min_value is not None else ''))
        if source_site == 'facebook':
            self._cursor.execute(facebook_query)
            self._connection.commit()

    def find_unique_place(self, place_name, address, city):
        self._cursor.execute(
            "select * "
            "from unique.place "
            "where name=%s and address=%s and city=%s;",
            (place_name, address, city))
        return self._cursor.fetchone()

    def insert_unique_place(self, place_name, address, city, geo_lat, geo_lng):
        self._cursor.execute(
            "insert ignore into unique.place"
            "(name, address, city, geo_lat, geo_lng) "
            "values (%s, %s, %s, %s, %s) ",
            (place_name, address, city, geo_lat, geo_lng))
        self._connection.commit()

    def insert_post_place_relation(self, post_id, place_id):
        self._cursor.execute(
            "insert ignore into unique.post_place"
            "(post_id, place_id) "
            "values (%s, %s) ",
            (post_id, place_id))
        self._connection.commit()

    def get_event_dicts(self, where_condition=None):
        query = (
            "select "
            "   post_id, post_title, post_content, event_time, place.name, "
            "   place.address, place.city, place.geo_lat, place.geo_lng "
            "from "
            "   unique.post "
            "   join unique.post_place using(post_id) "
            "   join unique.place using(place_id) " +
            (f"where {where_condition};"
                if where_condition is not None
                else ''))
        events = self.fetch_data(query)
        for event in events:
            event_dict = {
                'id': event[0],
                'name': event[1],
                'description': event[2],
                'place': {
                    'name': event[4], 'address': event[5], 'city': event[6],
                    'geo_lat': event[7], 'geo_lng': event[8]},
                'time': event[3]
            }
            yield event_dict


class PlaceGeocoder(Nominatim):
    """docstring for PlaceGeocoder"""

    # helper methods
    @staticmethod
    def _generate_place_queries(place_name, address, city):
        place_name = (place_name if place_name != '' else None)
        address = (address if address != '' else None)
        city = (city if city != '' else None)
        city_query = (city if city is not None else None)
        addreess_query = (
            f'{city}, {address}'
            if city is not None and address is not None
            else None)
        place_name_query = (
            f'{city}, {place_name}'
            if city is not None and place_name is not None
            else None)
        if addreess_query is None and place_name_query is None:
            return [city_query]
        else:
            return [
                query for query in (addreess_query, place_name_query)
                if query is not None]

    @staticmethod
    def _clean_place_query(place_query):
        clean_place_query = re.sub(
            "[A-Z]\. |\s(?=[a-zA-Z]$)"
            "|\(.*?\)"
            "|(?<=,).*?ul\.|(?<=,).*?,ul(?=)"
            "|, 3-3a|, pok\..*|, III p\."
            "|im\. gen\. Jerzego |[śŚ]w\. |[gG]en\. |[kK]s\. |[dD]r\. "
            "|(?<=Rady Europy) 1|(?<=Pod Lipami) 1"
            "|(?<=[a|b|c|d|\d]), os. .+lecia"
            "|(?<=Teatr) Lalki i Aktora"
            "|(?<=Filharmonia Śląska).*"
            "|(?<=Rondo Sztuki).*"
            "|(?<=Muzeum Historii Katowic).*"
            "|(?<=Wydział Teologiczny).*"
            "|(?<=Wydział Prawa i Administracji).*"
            "|(?<=Altus).*"
            "|(?<=Biblioteka Śląska).*"
            "|(?<=,).*?(?= Hipnoza)"
            "|(?<=,).*?(?= Muzeum Śląskie)"
            "|(?<=Muzeum Śląskie).*"
            "|(?<=,).*?(?= Old Timers Garage)"
            "|(?<=Ja)k(?=giellońska)"
            "|(?<=Warszaw)a(?=ska)"
            "|(?<=,).*?(?= Spodek)"
            "|(?<=,).*?(?= 3 Stawy)"
            "|(?<=,).*?(?= Rondo Sztuki)"
            "|, Katowice|w Katowicach"
            "|Trasia|, obok.*",
            "",
            place_query)
        return clean_place_query

    # public method(s)
    def geocode(
        self, place_name, address, city, exactly_one=True,
            timeout=5, addressdetails=False, language=False,
            geometry=None):
        logging.info(
            f'Place data input: '
            f'{place_name}, {address}, {city}')
        queries = self._generate_place_queries(place_name, address, city)
        for query in queries:
            time.sleep(1)
            query = self._clean_place_query(query)
            logging.info(f'Submitted query: {query}')
            location = super(PlaceGeocoder, self).geocode(
                query, exactly_one=exactly_one, timeout=timeout,
                addressdetails=addressdetails, language=language,
                geometry=geometry)
            if location is None:
                logging.info(
                    'Nominatim could not find geocode for the query.')
            else:
                logging.info(f'Returned location: {str(location)}\n')
                break
        if location is None:
            logging.warning(
                'could not find geocodes for place data: '
                f'{place_name}, {address}, {city}\n')
        return location


class EventAnalyzer(object):
    """docstring for EventAnalyzer"""
    def __init__(self, event_relation_model=None):
        self._event_relation_model = event_relation_model

    # helper methods
    @staticmethod
    def _clean_time_info(time_info):
        time_info = re.sub(', godz. 00.00| 00:00:00|^\d{1}–', '', time_info)
        time_info = re.sub('([^\d]|^)(\d{1}\.\d{2}\.\d{4})', '0\\2', time_info)
        time_info = re.sub(
            '^(\d{2}\.\d{2}\.\d{4})(, godz. \d{2}\.\d{2})'
            '( – \d{2}\.\d{2}\.\d{4})$',
            '\\1\\3',
            time_info)
        time_info = re.sub(
            '^(\d{2}\.\d{2}\.\d{4})(, godz. \d{2}\.\d{2})'
            '( – )'
            '(\d{2}\.\d{2}\.\d{4})(, godz. \d{2}\.\d{2})$',
            '\\1\\3\\4',
            time_info)
        time_info = re.sub(
            '(\d{2})\.(\d{2})\.(\d{4})',
            '\\3-\\2-\\1',
            time_info)
        time_info = re.sub(
            ', godz\. (\d{2})\.(\d{2})',
            ' \\1:\\2:00',
            time_info)
        return time_info

    @staticmethod
    def _get_time_info_pattern(clean_time_info):
        time_info_pattern = re.sub('\d', '_', clean_time_info)
        if time_info_pattern == '____-__-__ __:__:__':
            pattern = 'single_hour'
            order = 1
        elif time_info_pattern == '____-__-__':
            pattern = 'single_day'
            order = 3
        elif time_info_pattern == '____-__-__ – ____-__-__':
            pattern = 'span_day'
            order = 0
        elif '____-__-__ __:__:__|____-__-__ __:__:__' in time_info_pattern:
            pattern = 'multi_hour'
            order = 2
        elif '____-__-__|____-__-__' in time_info_pattern:
            pattern = 'multi_day'
            order = 4
        else:
            pattern = 'other'
            order = 5
        return {'pattern': pattern, 'order': order}

    @staticmethod
    def _extract_days(clean_time_info):
        days = re.findall('\d{4}-\d{2}-\d{2}', clean_time_info)
        return sorted(days)

    @staticmethod
    def _extract_hours(clean_time_info):
        hours = re.findall(
            '\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}',
            clean_time_info)
        return sorted(hours)

    def _process_time_info(self, time_info):
        clean_time = self._clean_time_info(time_info)
        pattern = self._get_time_info_pattern(clean_time)['pattern']
        order = self._get_time_info_pattern(clean_time)['order']
        days = self._extract_days(clean_time)
        hours = self._extract_hours(clean_time)
        return {
            'data': time_info,
            'pattern': pattern,
            'order': order,
            'days': days,
            'hours': hours}

    # public methods
    @staticmethod
    def compare_places(place_1, place_2):
        """place_1 and place_2 should be dicts with keys:
        name, address, city, geo_lat, geo_lng
        """
        place_1_description = (
            place_1['name'].lower(), place_1['address'].lower(),
            place_1['city'].lower())
        place_2_description = (
            place_2['name'].lower(), place_2['address'].lower(),
            place_2['city'].lower())
        lat_diff = abs(place_1['geo_lat'] - place_2['geo_lat'])
        lng_diff = abs(place_1['geo_lng'] - place_2['geo_lng'])
        if place_1_description == place_2_description:
            identity, similarity = 1, 1
        elif lat_diff < 0.0001 and lng_diff < 0.0001:
            identity, similarity = 0, 1
        else:
            identity, similarity = 0, 0
        return {'identity': identity, 'similarity': similarity}

    @staticmethod
    def compare_texts(text_1, text_2):
        text_distance = (
            SequenceMatcher(None, text_1.lower(), text_2.lower())
            .ratio())
        return text_distance

    def compare_time_info(self, time_info_1, time_info_2):
        # preprocess
        times = [
            self._process_time_info(time_info)
            for time_info in (time_info_1, time_info_2)]
        times_sorted = sorted(times, key=lambda x: x['order'])
        time_info_1, time_info_2 = times_sorted
        min_day_1 = min(time_info_1['days'])
        max_day_1 = max(time_info_1['days'])
        min_day_2 = min(time_info_2['days'])
        max_day_2 = max(time_info_2['days'])
        days_intersect = set(time_info_1['days']) & set(time_info_2['days'])
        hours_intersect = set(time_info_1['hours']) & set(time_info_2['hours'])
        # time identity
        if (time_info_1['pattern'] == time_info_2['pattern'] and
                time_info_1['days'] == time_info_2['days'] and
                time_info_1['hours'] == time_info_2['hours']):
            identity = 1
        else:
            identity = 0
        # time similarity
        similarity = 0
        if time_info_1['pattern'] == 'span_day':
            if time_info_2['pattern'] == 'span_day':
                if min_day_1 < min_day_2:
                    if max_day_1 >= min_day_2:
                        similarity = 1
                elif min_day_1 == min_day_2:
                    similarity = 1
                else:
                    if max_day_2 >= min_day_1:
                        similarity = 1
            else:
                for day in time_info_2['days']:
                    if day >= min_day_1 and day <= max_day_1:
                        similarity = 1
                        break
        elif time_info_1['pattern'] in ('single_hour', 'multi_hour'):
            if time_info_1['pattern'] in ('single_hour', 'multi_hour'):
                if len(hours_intersect) > 0:
                    similarity = 1
            else:
                if len(days_intersect) > 0:
                    similarity = 1
        else:
            if len(days_intersect) > 0:
                similarity = 1
        # return
        return {'identity': identity, 'similarity': similarity}

    def compare_events(self, event_1, event_2, model=None):
        """Events should be dicts """
        place_comparison = self.compare_places(
            event_1['place'], event_2['place'])
        time_comparison = self.compare_time_info(
            event_1['time'], event_2['time'])
        name_comparison = self.compare_texts(
            event_1['name'], event_2['name'])
        description_comparison = self.compare_texts(
            event_1['description'], event_2['description'])
        if (time_comparison['identity'] +
                place_comparison['identity'] == 2):
            timeplace_identity = 1
            timeplace_similarity = 1
        else:
            timeplace_identity = 0
            timeplace_similarity = 0
        if timeplace_similarity == 0:
            if (time_comparison['similarity'] +
                    place_comparison['similarity'] == 2):
                timeplace_similarity = 1
        if model is not None:
            prob_of_identity = model.predict_proba(np.array([
                place_comparison['similarity'],
                place_comparison['identity'],
                time_comparison['similarity'],
                time_comparison['identity'],
                timeplace_similarity,
                timeplace_identity,
                name_comparison,
                description_comparison]).reshape(1, -1))[0, 1]
        else:
            prob_of_identity = None
        return {
            'event_1':
                {k: v for k, v in event_1.items() if k != 'description'},
            'event_2':
                {k: v for k, v in event_2.items() if k != 'description'},
            'place_similarity': place_comparison['similarity'],
            'place_identity': place_comparison['identity'],
            'time_similarity': time_comparison['similarity'],
            'time_identity': time_comparison['identity'],
            'timeplace_similarity': timeplace_similarity,
            'timeplace_identity': timeplace_identity,
            'name_similarity': name_comparison,
            'description_similarity': description_comparison,
            'prob_of_identity': prob_of_identity
        }


# Scripts, examples
def transfer_silesiakultura_posts():
    pipeline = MysqlPipeline()
    pipeline.transfer_posts(source_site='silesiakultura')


def transfer_facebook_posts():
    pipeline = MysqlPipeline()
    pipeline.transfer_posts(source_site='facebook')


def extract_places_from_posts():
    pipeline = MysqlPipeline()
    geocoder = PlaceGeocoder()
    places = pipeline.fetch_data(
        'select '
        '   event_place_name, event_address, event_city, '
        '   event_geo_lat, event_geo_lng '
        'from unique.post ')
    for place in places:
        name, address, city = place[0], place[1], place[2]
        place_lookup = pipeline.find_unique_place(name, address, city)
        if place_lookup is None:
            place_geocodes = geocoder.geocode(name, address, city)
            if place_geocodes is not None:
                place_geo_lat = place_geocodes.latitude
                place_geo_lng = place_geocodes.longitude
            else:
                place_geo_lat = place[3]
                place_geo_lng = place[4]
            if place_geo_lat is not None and place_geo_lng is not None:
                pipeline.insert_unique_place(
                    name, address, city, place_geo_lat, place_geo_lng)


def post_place_relation():
    pipeline = MysqlPipeline()
    post_places = pipeline.fetch_data(
        'select post_id, event_place_name, event_address, event_city '
        'from unique.post')
    for post_place in post_places:
        post_id, place_name, address, city = post_place[0:4]
        place = pipeline.find_unique_place(place_name, address, city)
        if place is not None:
            place_id = place[0]
            pipeline.insert_post_place_relation(post_id, place_id)


def test_time_analyzer():
    pipeline = MysqlPipeline()
    analyzer = EventAnalyzer()
    times = pipeline.fetch_data(
        "select event_time from unique.post where post_id%50 = 0")
    for time_1 in times:
        for time_2 in times:
            comparison = analyzer.compare_time_info(time_1[0], time_2[0])
            print(comparison)


def test_event_comparison():
    pipeline = MysqlPipeline()
    analyzer = EventAnalyzer()
    model = pickle.load(open('../data/model_dump.pkl', 'rb'))
    event_dicts = pipeline.get_event_dicts(
        where_condition="event_time like '%12.2016%' "
        "or event_time like '%2016-12%'")
    event_list = list(event_dicts)
    start_row = 0
    for event_1 in event_list:
        start_row += 1
        for event_2 in event_list[start_row:]:
            comparison = analyzer.compare_events(event_1, event_2, model)
            if comparison['prob_of_identity'] > 0.1:
                print(comparison, '\n')
            # with open('../data/event_comparison.csv', 'a') as f:
            #     dict_writer = csv.DictWriter(f, comparison.keys())
            #     dict_writer.writerow(comparison)


if __name__ == '__main__':
    test_event_comparison()
