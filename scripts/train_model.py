
# imports
import pandas as pd
import numpy as np
import pickle
from sklearn.linear_model import LogisticRegression

# settings
np.set_printoptions(suppress=True)

# data
train = pd.read_csv('../data/training_sample.csv')
X, y = train.ix[:, 2:-1], train.ix[:, -1]

# train model
model = LogisticRegression(penalty='l2')
model.fit(X, y)
predictions = model.predict_proba(X)

# save predictions and model
np.savetxt(
    '../data/predictions.csv',
    predictions[:, 1], fmt='%f')
with open('../data/model_dump.pkl', 'wb') as f:
    pickle.dump(model, f)
